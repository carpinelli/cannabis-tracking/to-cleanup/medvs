""" Imports 
# Debugging and Logging
import traceback
import time
import logging
import sys

# Utilities
import datetime
import shutil
import os
import re
import pprint
import math
from copy import copy
from operator import itemgetter

# Excel File Manipulation
import openpyxl
from openpyxl.utils import get_column_letter
 End Imports """


################################ oldValidate.py ###############################
"""
def sortRows(Workbook, l_Sheet_Name):
  Sheet = Workbook[l_Sheet_Name]

  read_rows = list() # create a list at function scope
  sorted_rows = list() # create a sorted list at function scope

  for row in Sheet.iter_rows(min_row = Row_Start, max_row = Row_End,max_col = Column_End, values_only = True):
    #logger.debug("Adding row %s" %(str(row)))
    read_rows.append(row)

    # Remove None values from the list

    for sublist in read_rows:
      # Remove None values from sublist
      filtered_list = [row for row in sublist if row is not None]

      # Check if any values are left
      if len(filtered_list) > 0:
        sorted_rows.append(sublist) # append if list is left

      filtered_list.sort(key = get4thKey)

  logger.debug(sorted_rows)

  logger.info("Saving data to temporary file...")
  logger.info(TMP_File_Path)

  # Save Changes

  try:
    Workbook.save(TMP_File_Path)

  except:
    logger.critical("Error saving changes!")
    logger.critical("Column not capitalized!")
    logger.critical("Attempted to save data at %s" %(TMP_File_Path))

    return str()

  logger.info("Changes saved to %s" %(TMP_File_Path))

  return TMP_File_Path

# Takes an openpyxl cell
# Returns
#
def validRFID(cell):
  if cell.value is not None:
    return (str(cell.value).isalnum and (len(str(cell.value)) == RFID_Length))

  else:
    return True

#
#
#
def countErrors(Workbook, l_Sheet_Name, Cell_Range = RFID):
  error_list = {} # create empty dictionary in function scope
  Sheet = Workbook[l_Sheet_Name]

  for row in Sheet[Cell_Range]:
    for cell in row:
      if validRFID(cell) == False:
        logger.info("Found error at %s..." % (cell.coordinate))

        value = str(cell.value).strip()

        error_list.setdefault(value, 1)
        error_list[value] += 1

        logger.info("Added error %s" %(value))

  return error_list
"""
################################ oldValidate.py ###############################

################################ oldValidate.py ###############################
"""
def get4thKey(item):
  return str(item[4])

def sortRows(Workbook, l_Sheet_Name):
  Sheet = Workbook[l_Sheet_Name]

  read_rows = list() # create a list at function scope
  sorted_rows = list() # create a sorted list at function scope

  for row in Sheet.iter_rows(min_row = Row_Start, max_row = Row_End,max_col = Column_End, values_only = True):
    #logger.debug("Adding row %s" %(str(row)))
    read_rows.append(row)

    # Remove None values from the list

    for sublist in read_rows:
      # Remove None values from sublist
      filtered_list = [row for row in sublist if row is not None]

      # Check if any values are left
      if len(filtered_list) > 0:
        sorted_rows.append(sublist) # append if list is left

      filtered_list.sort(key = get4thKey)

  logger.debug(sorted_rows)

  logger.info("Saving data to temporary file...")
  logger.info(TMP_File_Path)

  # Save Changes

  try:
    Workbook.save(TMP_File_Path)

  except:
    logger.critical("Error saving changes!")
    logger.critical("Column not capitalized!")
    logger.critical("Attempted to save data at %s" %(TMP_File_Path))

    return str()

  logger.info("Changes saved to %s" %(TMP_File_Path))

  return TMP_File_Path

# Takes an openpyxl cell
# Returns
#
def validRFID(cell):
  if cell.value is not None:
    return (str(cell.value).isalnum and (len(str(cell.value)) == RFID_Length))

  else:
    return True

#
#
#
def countErrors(Workbook, l_Sheet_Name, Cell_Range = RFID):
  error_list = {} # create empty dictionary in function scope
  Sheet = Workbook[l_Sheet_Name]

  for row in Sheet[Cell_Range]:
    for cell in row:
      if validRFID(cell) == False:
        logger.info("Found error at %s..." % (cell.coordinate))

        value = str(cell.value).strip()

        error_list.setdefault(value, 1)
        error_list[value] += 1

        logger.info("Added error %s" %(value))

  return error_list
"""
################################ oldValidate.py ###############################
