"""path module."""


import sys
import time
import os

from medvs import excel, utils


""" Constants """
# Directories
Working_Directory = os.path.abspath(os.path.dirname(sys.argv[0]))
# was __file__
Output_Directory = os.path.abspath(os.path.join(Working_Directory,
                                                "..",
                                                "Output Files"))

# Files
Tmp_File = os.path.join(Working_Directory, "tmp.xlsx")
Deletable_File = os.path.join(Working_Directory, "Safe_to_DELETE_ME.txt")
Log_File = os.path.join(Working_Directory, "debug.log")
# Validated_File_Path = out_filepath[:-5] + "_Validated.xlsx"

# File Extensions
CSV_Extension = ".csv"
XLSX_Extension = ".xlsx"
""" End Constants """


""" Functions """


def exists(filepath):
    """Depracted!"""
    return os.path.exists(filepath)


def getParentFilepath(in_filepath):
    """Takes a filepath and checks the parent directory for the file,
    Returns the path if found.
    Returns None if the file is not found.
    LOGGER FOR FILE PATH.
    """

    basename = os.path.basename(in_filepath)  # get just filename
    directoryname = os.path.dirname(in_filepath)  # get just the directoryname

    # Check for same file in the parent directory
    # logger.info("Checking parent directory for %s" % (basename))
    out_filepath = os.path.abspath(os.path.join(directoryname, os.pardir))
    out_filepath = os.path.join(out_filepath, basename)

    # If no file found, return None
    if not os.path.exists(out_filepath):
        # logger.info("File %s not found..." % (out_filepath))
        out_filepath = None

    return out_filepath


def getFilepath():
    """Gets filename from the user and checks if it exists,
    Returns a string containing the path of the filename.
    """

    # logger.info("Taking user input...")

    try:
        in_filename = input("Enter the excel file's name: ")

    except OSError as current_exception:
        print(current_exception)
        # Log input failure
        # logger.critical("Failed to receive or parse input!")
        # logger.critical(str(traceback.format_exc()))
        # logger.critical("Exiting program in 20 seconds...\n")
        time.sleep(20)
        sys.exit(-1)

    # Detect if extension is missing and add if so
    # logger.debug("Checking if Excel extension was provided...")
    if in_filename[-5:] != excel.Extension:
        # logger.info("Adding Excel file extension: %s" % (excel.Extension))
        in_filename += excel.Extension

    out_filepath = os.path.join(Working_Directory, in_filename)

    # If file does not exist

    # logger.info("Checking if %s exists..."
    #             % (os.path.basename(out_filepath)))

    if not os.path.exists(out_filepath):
        # logger.info("File %s not found..." % (out_filepath))
        out_filepath = getParentFilepath(out_filepath)

    return out_filepath


def getDetectedFilepath(in_Leading_Zeros=True):
    """Returns a filepath based on today's date."""
    date = utils.getFormattedDate(in_Leading_Zeros)

    Detected_Basename = "Harvest " + date + excel.Extension

    # logger.info("Getting path for %s..." % (Detected_Basename))
    out_detected_filepath = os.path.join(Working_Directory, Detected_Basename)

    # logger.info("Checking if %s exists..." % (out_detected_filepath))
    if not os.path.exists(out_detected_filepath):
        # logger.info("File %s not found..." % (out_detected_filepath))
        out_detected_filepath = getParentFilepath(out_detected_filepath)

    return out_detected_filepath


""" End Functions """
