#! /usr/bin/env python3

""" Imports """
# Debugging and Logging
import traceback
import time
import logging
import sys

# Utilities
import os
import re
from operator import itemgetter
from pathlib import Path

# Excel File Manipulation
import openpyxl
from openpyxl.utils import get_column_letter

try:
    import medvs  # if package is installed

except ImportError as current_exception:
    print(current_exception)
    print("medvs is not installed")
    print("Running as portable")

    sys.path.append(os.path.join(os.path.dirname(sys.argv[0]),
                                 ".."))
    import medvs
""" End Imports """

""" ValidateHarvest.py
Description: Validate TGS Steele Street Harvest Team File
Maintained By: Joseph Carpinelli
Last Updated: June 7, 2019
"""


""" Constants """
# Regular Expressions
Room_Regex = re.compile(r"([A-S])-(\d{1,2})")

# File Extensions
Excel_Extension = ".xlsx"

# OpenPyXL #
# Sheetnames
l_Weights = "Weights"
l_Report = "Report"

# Column Indices
RFID_Index = 0
Room_Index = 3

# Data Ranges
Start_Row = 3
End_Row = 1000
Start_Column = RFID_Index + 1
End_Column = Room_Index + 1

# Column Ranges
RFID = "A3:A1000"
WEIGHT = "B3:B1000"
STRAIN = "C3:C1000"
ROOM = "D3:D1000"  # Sheet Ranges
Weights_Range = "$A$3:$D$1000"

# Validation
RFID_Length = 24
RFID_Beginning = "(1A4000)(21266EDF4000)(XXXXXX)"
# End OpenPyXL #

# Path #

Input_Message = "Drag the harvest file into this window and press enter: "
harvest_file = input(Input_Message)
harvest_file = harvest_file.strip("'\" ")
if not os.path.exists(harvest_file):
    basename = os.path.basename(harvest_file)
    harvest_file = os.path.join(medvs.path.Working_Directory, basename)
    if not medvs.path.exists(harvest_file):
        harvest_file = os.path.join(medvs.path.Output_Directory, basename)
        if not medvs.path.exists(harvest_file):
            raise FileNotFoundError

# Directories
Working_Directory = os.path.abspath(os.path.dirname(__file__))

# Files
Log_File = os.path.join(Working_Directory, "debug.log")

# Formatting
Log_Format = "%(asctime)s - %(levelname)s - %(message)s"
# End Path #
""" End Constants """


""" Logger Setup """
# Setup Logger
try:
    logger = logging.getLogger(__name__)  # Get logger with module name

    file_handler = logging.FileHandler(Log_File)  # Get file handler
    stream_handler = logging.StreamHandler()  # Get stream handler

    formatter = logging.Formatter(Log_Format)  # Get formatter

    # Handle all message levels
    logger.setLevel(logging.DEBUG)
    file_handler.setLevel(logging.DEBUG)
    stream_handler.setLevel(logging.INFO)

    # Set formatter
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)

    # Add file handlers
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

except RuntimeWarning as current_exception:
    # Attempt to log setup failure
    logger.warning("Logger setup failed!")
    logger.warning(str(traceback.format_exc()))
    print(current_exception)
    print("EXITING NOW>>>")
    exit(1)

    # Notify setup failure outside logging
    print("An exception occured... Logging disabled.")

    logging.disable()  # Disable logging after failed setup
    """ End Logger Setup """


""" Functions """


def getSheetlist(in_Workbook, in_Sheetname):
    # Takes and openpyxl workbook and sheetname
    # Returns a worksheet as a list

    Sheet = in_Workbook[in_Sheetname]

    raw_rows = list()  # create a list at function scope
    out_filtered_rows = list()  # create a sorted list at function scope

    for row in Sheet.iter_rows(min_row=Start_Row,
                               max_row=End_Row,
                               max_col=End_Column,
                               values_only=True):
        logger.debug("Adding raw row %s" % (str(row)))
        raw_rows.append(row)

    # Remove None values from the list
    for sublist in raw_rows:
        # Remove None values from sublist
        tmp_filtered_rows = [row for row in sublist if row is not None]

        # Check if any values are left
        if (len(tmp_filtered_rows) > 0) and ((sublist[0] is not None) and
                                             (sublist[1] is not None)):
            logger.warning("Adding filtered row: %s" % (str(sublist)))
            logger.info("Adding filtered row: %s" % (str(sublist)))
            out_filtered_rows.append(sublist)  # append if list is left

    return out_filtered_rows


def getUppercaseSheetlist(in_Sheetlist):
    # Takes an openpyxl Workbook file and cell location information
    # Capitalizes the range of cells specified in the argument parameters
    # Returns a string containing the path to the capitalized file

    logger.critical("Row len (uppercase before): %s"
                    % (str(len(in_Sheetlist))))
    out_sheetlist = list()  # create return variable at scope

    for row_number, row in enumerate(in_Sheetlist, start=1):
        uppercase_row = list()  # hold capitalized rows before appending

        for column_number, cell_value in enumerate(row, start=1):
            if cell_value is None:
                cell_value = ""

            # capitalize all characters
            logger.info("Changing %s to uppercase..."
                        % (get_column_letter(column_number) + str(row_number)))
            uppercase_row.append(str(cell_value).upper())

            logger.info("Changed %s to %s"
                        % (get_column_letter(column_number)
                           + str(row_number),
                           uppercase_row[column_number - 1]))

        out_sheetlist.append(uppercase_row)

    logger.critical("Row len (uppercase after): %s"
                    % (str(len(out_sheetlist))))

    return out_sheetlist


def getRoomGroups(in_row):
    # Takes a row of text from getSheetlist
    # Returns a list containing the letter and number of the room
    # If match is not found
    # Returns a list containing an empty string and then a 0
    regex_match = Room_Regex.search(str(in_row[Room_Index]))

    if regex_match is not None:
        return [regex_match.group(1), int(regex_match.group(2))]

    else:
        return ["", 0]


def f_getRoomLetter(in_room_split): return getRoomGroups(in_room_split)[0]
# Takes a list containing the letter and then number of a room
# Returns the letter


def f_getRoomNumber(in_room_split): return getRoomGroups(in_room_split)[1]
# Takes a list containing the letter and then number of a room
# Returns the number


def getSortedSheetlist(in_row_list):
    # Returns a sorted 2D list
    logger.info("Sorting by RFID as tertiary sort...")

    # tertiary sort by RFID, as new list
    out_sorted_rows = sorted(in_row_list, key=itemgetter(RFID_Index))

    # Sorting by room

    logger.info("Sorting by room...")

    # secondary sort, reversed
    logger.info("Sorting by room number as secondary sort...")
    out_sorted_rows.sort(key=f_getRoomNumber)

    # primary sort by room letter
    logger.info("Sorting by room letter as primary sort...")
    out_sorted_rows.sort(key=f_getRoomLetter)

    return out_sorted_rows


def saveListAs(in_workbook, in_Sheetname, in_rows, in_Validated_File_Path):
    # Takes a Workbook, a Sheetname, and a 2D list to insert
    # Saves 2D as an Excel Spreadsheet
    # Returns None if succesful, if fail, throws exception and exits

    Sheet = in_workbook[in_Sheetname]
    """
    logger.info("Getting column styles...")
    Column_Styles = [copy(Sheet[RFID[:2]].style),
    copy(Sheet[WEIGHT[:2]].style),
    copy(Sheet[CORRECTED[:2]].style),
    copy(Sheet[STRAIN[:2]].style),
    copy(Sheet[ROOM[:2]].style)]
    """
    # Clears column C
    # Sheet.delete_cols(Corrected_Index + 1)

    # Set each item to corresponding cell
    for row_number, row in enumerate(in_rows, start=Start_Row):
        for column_number, cell_value in enumerate(row, start=1):
            cell_coordinate = get_column_letter(column_number)
            cell_coordinate += str(row_number)
            cell = Sheet[cell_coordinate]

            """
            # Get copies of cell styles
            if cell.has_style:
            cell_font = copy(cell.font)
            cell_border = copy(cell.border)
            cell_fill = copy(cell.fill)
            cell_number_format = copy(cell.number_format)
            cell_protection = copy(cell.protection)
            cell_alignment = copy(cell.alignment)
            """

            # skip empty values
            if (cell_value == "") or (cell_value is None):
                continue

            # convert value to proper data type
            if column_number == 2:
                Sheet[cell_coordinate].value = float(cell_value)

            else:
                Sheet[cell_coordinate].value = cell_value

            """
            # Set cell styles
            if cell.has_style:
            cell.font = cell_font
            cell.border = cell_border
            cell.fill = cell_fill
            cell.number_format = cell_number_format
            cell.protection = cell_protection
            cell.alignment = cell_alignment
            """

            logger.info("Changed %s to %s"
                        % (cell_coordinate, str(cell.value)))
        # End for
    # End for

    # Clear cells below data

    data_end = len(in_rows) + Start_Row

    logger.info("Clearing original worksheet from row %s to row %s"
                % (data_end, End_Row))
    Sheet.delete_rows(data_end, End_Row - data_end)

    # Save Validated Workbook

    logger.info("Saving validated spreadsheet...")

    try:
        in_workbook.save(in_Validated_File_Path)

    except OSError as current_exception:
        logger.critical(current_exception)
        logger.critical(str(traceback.format_exc()))
        logger.critical("Unable to save file!")
        logger.critical("The filename attempted was %s"
                        % (in_Validated_File_Path))
        logger.critical("Exiting program in 2 seconds...\n")
        time.sleep(2)
        sys.exit(-1)

    logger.info("Saved at %s" % (in_Validated_File_Path))

    return None


""" End Functions """


""" Main """


def main():
    logger.info("Started program...")

    # Get Filename

    Validated_Filename = (os.path.basename(harvest_file)[:-5]
                          + "_Validated.xlsx")
    Validated_File_Path = (Path(medvs.path.Output_Directory)
                           / Validated_Filename)

    # Load Workbook

    logger.info("Loading workbook %s..." % (harvest_file))
    try:
        spreadsheet = openpyxl.load_workbook(harvest_file, data_only=True)

    except FileNotFoundError as current_exception:
        logger.critical(current_exception)
        logger.critical(str(traceback.format_exc()))
        logger.critical("Unable to load excel file!")
        logger.critical("The filename used was %s" % (harvest_file))
        logger.critical("Exiting program in 20 seconds...\n")
        sys.exit(-1)

    logger.info("Workbook successfully loaded")

    # Convert worksheet into 2D list
    sheetlist = getSheetlist(spreadsheet, l_Weights)

    # Change sheet to uppercase
    capitalized_rows = getUppercaseSheetlist(sheetlist)

    # Sort
    validated_rows = getSortedSheetlist(capitalized_rows)

    """
    # Print
    print("Here is the validated list:")
    pprint.pprint(validated_rows)
    print('\n')
    """

    # Saving Validated Spreadsheet
    saveListAs(spreadsheet, l_Weights, validated_rows, Validated_File_Path)

    os.system("genHarvestCSV.py" + ' "' + str(Validated_File_Path) + '"')

    logger.info("Exiting program in 2 seconds...\n")
    time.sleep(2)
    """ End Main """


""" Call main() """
if __name__ == "__main__":
    main()
""" End Call main() """
