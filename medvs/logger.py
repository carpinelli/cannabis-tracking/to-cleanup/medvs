"""logger module"""

import logging
import traceback

from medvs import path

""" Constants """
# Formatting
Log_Format = "%(asctime)s - %(levelname)s - %(message)s"
""" End Constants """

""" Logger Setup """
# Setup Logger
try:
    logger = logging.getLogger(__name__)  # Get logger with module name

    file_handler = logging.FileHandler(path.Log_File)  # Get file handler
    stream_handler = logging.StreamHandler()  # Get stream handler

    formatter = logging.Formatter(Log_Format)  # Get formatter

    # Handle all message levels
    logger.setLevel(logging.DEBUG)
    file_handler.setLevel(logging.DEBUG)
    stream_handler.setLevel(logging.WARN)

    # Set formatter
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)

    # Add file handlers
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

except RuntimeWarning as current_exception:
    logger.critical(current_exception)
    logger.critical(str(traceback.format_exc()))
    # Attempt to log setup failure
    logger.warn("Logger setup failed!")
    logger.warn(str(traceback.format_exc()))

    # Notify setup failure outside logging
    print("An exception occured... Logging disabled.")

    logging.disable()  # Disable logging after failed setup
""" End Logger Setup """
